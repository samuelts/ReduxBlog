# ReduxBlog

A blog site using React, Redux, and routing. 

Demo here: https://samuelts.com/ReduxBlog/

### Images
![Main View](https://raw.githubusercontent.com/safwyls/logos/master/ReduxBlog.png)
![View Post](https://raw.githubusercontent.com/safwyls/logos/master/ReduxBlog_show.png)
![New Post](https://raw.githubusercontent.com/safwyls/logos/master/ReduxBlog_new.png)
