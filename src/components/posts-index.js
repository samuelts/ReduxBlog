import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchPosts } from '../actions';

class PostsIndex extends Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  render() {
    const postList = _.map(this.props.posts, post => {
      return (
        <Link to={`/ReduxBlog/posts/${post.id}`}>
          <li key={post.id} className='list-group-item'>
            {post.title}
            <div className='listTags'>
              Tags: {post.categories}
            </div>
          </li>
        </Link>
      )
    })
    return (
      <div>
        <Link className='btn btn-primary pull-xs-right' to="/ReduxBlog/posts/new">
          Add a Post
        </Link>
        <h3>All Posts</h3>
        <h7>*This is a demo app, please note that all entered data is publicly accessible and unencrypted.</h7>
        <ul className='list-group'>
          {postList}
        </ul>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return { posts: state.posts };
}

export default connect(mapStateToProps, { fetchPosts })(PostsIndex);